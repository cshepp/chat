/* ES type templates */

var config = require('./config/config')();
var elasticsearch = require('elasticsearch');
var ESClient = elasticsearch.Client({ host: config.elasticsearch_host });
var password = require('password-hash-and-salt');

//first, create the static index
ESClient.indices.create({
    "index": config.elasticsearch_index_prefix + "-static",
    "mappings": {
        "user": {
            "properties": {
                "username":{ "type": "string", "index": "not_analyzed" },
                "password": { "type": "string", "index": "not_analyzed" },
                "image_path": { "type": "string", "index": "not_analyzed" },
                "settings": {
                    "type": "nested",
                    "properties": {
                        "channels_with_alerts": { "type": "string", "index": "not_analyzed" },
                        "auto_join_channels": { "type": "string", "index": "not_analyzed" },
                        "keywords": { "type": "string", "index": "not_analyzed" },
                        "mentions": { "type": "string", "index": "not_analyzed" },
                        "is_admin": { "type": "string", "index": "not_analyzed" },
                        "channel_settings": {
                            "type": "nested",
                            "properties": {
                                "channel_name": { "type": "string", "index": "not_analyzed" },
                                "notification_level": { "type": "string", "index": "not_analyzed" }
                            }
                        }
                    }
                }
            }
        },
        "channel": {
            "properties": {
                "name": { "type": "string", "index": "not_analyzed" },
                "active_parsers": { "type": "string", "index": "not_analyzed" }
            }
        }
    }
},
function(err, response){
    if(err) console.log(err);
    if(response) console.log(response);
});

//insert default user
password('bamboo').hash(function(err, hash){
    console.log(hash);
    ESClient.index({
        "index": config.elasticsearch_index_prefix + "-static",
        "type": "user",
        "body": {
            "username": "panda",
            "password": hash,
            "image_path": "",
            "settings": {
                "channels_with_alerts": ["general"],
                "auto_join_channels": ["general"],
                "keywords": [],
                "mentions": true,
                "is_admin": true,
                "channel_settings": [
                    {
                        "channel_name": "general",
                        "notification_level": "1"
                    }
                ]
            }
        }
    },
    function(err, response){
        if(err) console.log(err);
        if(response) console.log(response);
    });
});

//insert default channel(s)
ESClient.index({
    "index": config.elasticsearch_index_prefix + "-static",
    "type": "channel",
    "body": {
        "name": "general",
        "active_parsers": ["links", "images", "redmine"]
    }
},
function(err, response){
    if(err) console.log(err);
    if(response) console.log(response);
});


// We have multiple message indexes - one per day.
// Because of that, we're going to register an index template
// message indexes should be named as: {es-prefix}-messages-YYYY.MM.DD
ESClient.indices.putTemplate({
    "name": "message-template",
    "order": 0,
    "body": {
        "template": config.elasticsearch_index_prefix + "-messages*",
        "mappings": {
            "message": {
                "properties": {
                    "channel": { "type": "string", "index": "not_analyzed" },
                    "timestamp": { "type": "date", "index": "analyzed" },
                    "text": { "type": "string", "index": "analyzed" },
                    "username": { "type": "string", "index": "not_analyzed" }
                }
            }
        }
    }
},
function(err, response){
    if(err) console.log(err);
    if(response) console.log(response);
});

//@TODO
//ESClient.close(); when???