
angular.module('iris')
.factory('socket', function(socketFactory){
    var remote = "http://localhost:8080";
    //var remote = "http://192.168.33.127:8080";
    var ioSocket = io.connect(remote, { query: 'token=' + localStorage.token });
    return socketFactory({ ioSocket: ioSocket});
});