
//Local User Manager

angular.module('iris')
.service('LocalUserManager', ['socket', 'local_user', function(socket, local_user){

    var _self = this;
    this.user = null;
    this.user_id = localStorage._id;

    window.lum = this;

    this.getUser = function(){
        return this.user;
    };

    socket.on('user', function(data){
        if(data._id == _self.user_id && data.hasOwnProperty("settings")){
            if(_self.user === null){
                _self.user = local_user.create(this, data.username, data._id, data.image_path, data.settings);
            }
            else{
                _self.user.settings     = data.settings;
                _self.user.status       = data.status;
                _self.user.image_path   = data.image_path;
            }
        }
    });
}]);