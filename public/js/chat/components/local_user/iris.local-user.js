//LocalUser Factory

angular.module('iris')
.factory('local_user', ['socket', function(socket){

    var LocalUserFactory = {
        create: function(socket, username, _id, image_path, settings){
            return new LocalUser(socket, username, _id, image_path, settings);
        }
    };

    return LocalUserFactory;
}]);


function LocalUser(socket, username, _id, image_path, settings){

    var _self = this;
    this._id = _id;
    this.username = username;
    this.image_path = image_path;
    this.status = "online";
    this.settings = settings;
    this.socket = socket;

    //public methods
    this.getChannelsWithNotificationsSetting = function(){
        return this.settings.channels_with_alerts;
    };

    this.getMentionsSetting = function(){
        return this.settings.mentions;
    };

    this.getKeywordsSetting = function(){
        var keywordString = this.settings.keywords.reduce(function(pre, cur){
            if(pre.length === 0){
                return cur;
            }
            else{
                return pre + ", " + cur;
            }

        }, "");

        return keywordString;
    };

    this.saveToServer = function(){
        //@TODO: implement this server-side
        this.socket.emit('*', { type: 'update-user', document_id: this.document_id, keywords: this.settings.keywords, mentions: this.settings.mentions, channels_with_alerts: this.settings.channels_with_alerts, image_path: this.image_path });
    };

    this.channelNotificationsEnabled = function(channel_name){
        return (this.settings.channels_with_alerts.indexOf(channel_name) >= 0);
    };

    this.updateUserStatus = function(new_status){
        this.status = new_status;
        console.log(socket);
        this.socket.emit('*', { type: 'update-user-status', body: { _id: this._id, status: new_status }});
    };
}