//Remote User Manager

angular.module('iris')
.service('RemoteUserManager', ['socket', 'remote_user', function(socket, remote_user){

    var _self = this;
    this.local_user_id = "";
    this.users = [];

    this.getUsers = function(){
        return this.users;
    };

    socket.on('user', function(data){
        //we're only interested in users that are not the local user
        if(data._id !== _self.local_user_id){
            if(_self.user === null){
                _self.user = local_user.create(this, data.username, data._id, data.image_path, data.settings);
            }
            else{
                _self.user.settings     = data.settings;
                _self.user.status       = data.status;
                _self.user.image_path   = data.image_path;
            }
        }
    });
}]);