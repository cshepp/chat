
//User Factory

angular.module('iris')
.service('user', ['socket', function(socket){

    var _self = this;
    this.document_id = "";
    this.username = "";
    this.image_path = "";
    this.status = "online";
    this._id = localStorage._id;
    this.settings = {
        auto_join_channels: [],
        channels_with_alerts: [],
        is_admin: false,
        keywords:[],
        mentions: false,
        channel_settings: []
    };

    //public methods
    this.init = function(){
        socket.emit('*', { type: 'register-user', _id: this._id });
    };

    this.getChannelsWithNotificationsSetting = function(){
        return this.settings.channels_with_alerts;
    };

    this.getMentionsSetting = function(){
        return this.settings.mentions;
    };

    this.getKeywordsSetting = function(){
        var keywordString = this.settings.keywords.reduce(function(pre, cur){
            if(pre.length === 0){
                return cur;
            }
            else{
                return pre + ", " + cur;
            }

        }, "");

        return keywordString;
    };

    this.saveToServer = function(){
        //@TODO: implement this server-side
        socket.emit('*', { type: 'update-user', document_id: this.document_id, keywords: this.settings.keywords, mentions: this.settings.mentions, channels_with_alerts: this.settings.channels_with_alerts, image_path: this.image_path });
    };

    this.channelNotificationsEnabled = function(channel_name){
        return (this.settings.channels_with_alerts.indexOf(channel_name) >= 0);
    };

    this.updateUserStatus = function(new_status){
        this.status = new_status;
        socket.emit('*', { type: 'update-user-status', username: this.username, status: new_status });
    };

    //event handlers
    socket.on('client-active', function(data){
        console.log(data);//@TODO?
    });

    socket.on('user-settings', function(data){
        _self.document_id = data.document_id;
        _self.image_path = data.image_path;
        _self.settings.auto_join_channels = data.settings.auto_join_channels;
        _self.settings.channels_with_alerts = data.settings.channels_with_alerts;
        _self.settings.is_admin = data.settings.is_admin;
        _self.settings.keywords = data.settings.keywords;
        _self.settings.mentions = data.settings.mentions;
        _self.settings.channel_settings = data.settings.channel_settings;

        //auto-join the appropriate channels
        _self.settings.auto_join_channels.map(function(chan){
            socket.emit('local', { type: 'join-channel', channel_name: chan });
        });
    });
}]);