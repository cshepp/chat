
//User Factory

angular.module('iris')
.service('UserManager', ['socket', '$rootScope', function(socket, $rootScope){

    var _self = this;
    //this.users = [];

    $rootScope.$on('adminSettingsModalActive', function(){
        //_self.requestUserData();
    });

    this.getUsers = function(){
        return this.users;
    };

    this.requestUserData = function(){
        socket.emit('*', { type: 'request-all-user-data' });
    };

    socket.on('all-user-data', function(data){
        _self.users = data.users;
    });
}]);