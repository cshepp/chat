
angular.module('iris')
.service('ChannelManager', ['socket', 'channel', '$rootScope', function(socket, channel, $rootScope){

    //private props/methods
    var _self = this;

    //public props/methods
    this.channels = {};
    this.joined_channels = [];
    this.active_channel_name = "general";
    this.active_users = [];

    this.init = function(user){
        this.user = user;
        //socket.emit('*', { type: 'request-channel-list' });
    };

    this.getChannels = function(){
        return this.channels;
    };

    this.getActiveChannel = function(){
        if(this.channels.hasOwnProperty(this.active_channel_name)){
            return this.channels[this.active_channel_name];
        }
        
        return {};
    };

    this.getActiveChannelMessages = function(){
        if(this.channels.hasOwnProperty(this.active_channel_name)){
            return this.channels[this.active_channel_name].messages;
        }

        return [];
    };

    this.loadPreviousMessages = function(){
        //for active channel
        var messages = this.getActiveChannelMessages();
        var oldest_message = null;
        for(var i = 0; i < messages.length; i++){
            var m = messages[i];
            if(oldest_message === null || m.timestamp < oldest_message.timestamp){
                oldest_message = m;
            }
        }
        socket.emit('*', { type: 'get-previous-messages', channel_name: this.active_channel_name, from: oldest_message.timestamp });
    };

    this.changeActiveChannel = function(name){
        if(this.channels.hasOwnProperty(this.active_channel_name)){
            this.channels[this.active_channel_name].active = false;
        }

        this.active_channel_name = name;
        this.channels[this.active_channel_name].active = true;
        this.channels[this.active_channel_name].unread = 0;
        if(this.joined_channels.indexOf(name) < 0){
            socket.emit('local', { type: 'join-channel', channel_name: name });
        }

        $rootScope.$broadcast('scrollMessages');
    };

    this.leaveActiveChannel = function(){
        socket.emit('local', { type: 'leave-channel', channel_name: this.active_channel_name });
    };

    this.getActiveUsers = function(){
        console.log("Active users", this.active_users);
        return this.active_users;
    };

    socket.on('channel', function(data){
        console.log("channel", data)
    });

    //event handlers
    socket.on('new-channel', function(data){
        console.log(data);
        var c = channel.create(data.name, data.active_parsers, [], true, data.document_id);
        if(data.name == 'general'){
            c.active = true;
            c.unread = 0;
        }
        c.getUserSettings();
        if(! _self.channels.hasOwnProperty(data.channel_name)){
            _self.channels[data.name] = c;
            //@NOTE: message-backfill is automatically sent when joining a channel
        }
    });

    socket.on('message', function(data){
        var target_channel = _self.channels[data.channel_name];
        target_channel.newMessage(data.message, false);
    });

    socket.on('previous-messages', function(data){
        if(_self.channels.hasOwnProperty(data.channel_name)){
            var target_channel = _self.channels[data.channel_name];
            data.messages.map(function(m){
                target_channel.newMessage(m, true);
            });
        }
    });

    socket.on('message-backfill', function(data){
        console.log(data);
        if(_self.channels.hasOwnProperty(data.channel_name)){
            var target_channel = _self.channels[data.channel_name];
            data.messages.map(function(m){
                target_channel.newMessage(m, true);
            });
        }
    });

    socket.on('user-joined-channel', function(data){
        if(data.username == _self.user.username){
            //we joined this channel
            //console.log("we joined a channel");
            if(_self.joined_channels.indexOf(data.channel_name) < 0){
                _self.joined_channels.push(data.channel_name);
                _self.channels[data.channel_name].joined = true;
            }
            else{
                console.warn("double channel join", data);
            }
        }
        else{
            //keep track of other users in this channel?
        }
    });

    socket.on('user-left-channel', function(data){
        if( data.username == _self.user.username &&
            _self.joined_channels.indexOf(data.channel_name) >= 0){

            _self.joined_channels.splice(_self.joined_channels.indexOf(data.channel_name), 1);
            _self.channels[data.channel_name].joined = false;
            _self.channels[data.channel_name].active = false;

            if(data.channel_name == _self.active_channel_name){

                if(_self.joined_channels.length > 0){
                    _self.active_channel_name = _self.joined_channels[0];
                    _self.channels[_self.active_channel_name].active = true;
                    _self.channels[_self.active_channel_name].unread = 0;
                }
                else{
                    //????
                }
            }
        }
        else{

        }
    });

    socket.on('channel-deleted', function(data){
        if(_self.channels.hasOwnProperty(data.channel_name)){
            delete _self.channels[data.channel_name];
            if(_self.active_channel_name == data.channel_name){
                _self.changeActiveChannel(_self.channels[Object.keys(_self.channels)[0]].name);
            }
        }
    });

    socket.on('new-active-user', function(data){
        if(data.username != _self.user.username){
            var usernames = _self.active_users.map(function(u){ return u.username; });

            if(usernames.indexOf(data.username) < 0){
                _self.active_users.push({username: data.username, status: "online"});
            }
        }
    });

    socket.on('updated-user-status', function(data){
        if(data.username != _self.user.username){
            for(var i = 0; i < _self.active_users.length; i++){
                if(_self.active_users[i].username == data.username){
                    _self.active_users[i].status = data.status;
                }
            }
        }
    })
}]);