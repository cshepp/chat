
angular.module('iris')
.factory('channel', ['socket', 'LocalUserManager', '$rootScope', 'NotificationManager', function(socket, LocalUserManager, $rootScope, NotificationManager){

    var ChannelFactory = {
        create: function(name, active_parsers, messages, show_notifications, document_id){
            return new Channel(socket, LocalUserManager, $rootScope, NotificationManager, name, active_parsers, messages, show_notifications, document_id);
        }
    };

    return ChannelFactory;
}]);

function Channel(socket, LocalUserManager, $rootScope, NotificationManager, name, active_parsers, messages, show_notifications, document_id){
    this.document_id = document_id;
    this.socket = socket;
    this.user = LocalUserManager.user;
    this.$rootScope = $rootScope;
    this.NotificationManager = NotificationManager;
    this.name = name;
    this.active_parsers = active_parsers;
    this.unread = 0;
    this.messages = messages;
    this.show_notifications = show_notifications;
    this.active = false;
    this.joined = false;
}

Channel.prototype.newMessage = function(message, isBackfill){
    //parse message
    var m = this.parseMessageForAttachments(message);

    if(!isBackfill ){//never emit notifications for backfilled messages // && message.username != this.user.username
        //check to see if we should emit a notification
        if( this.user.channelNotificationsEnabled(this.name) &&
            this.show_notifications){

            console.log("general channel notification!", message);
            this.NotificationManager.emitNotification(message.text, message.username, this.name, message.timestamp);
        }
        else if(this.user.settings.mentions && m.text.indexOf(this.user.username) >= 0){
            //mention
            console.log("mention notification!");
            this.NotificationManager.emitNotification(message.text, message.username, this.name, message.timestamp);
        }
        else if(this.user.settings.keywords.length > 0 && this.user.settings.keywords[0] !== ''){
            var r = this.user.settings.keywords.reduce(function(previous, current){
                if(m.text.indexOf(current) >= 0){
                    return previous + 1;
                }
                else{
                    return previous;
                }
            }, 0);

            if(r > 0){
                //keyword
                console.log("keyword notification!");
                this.NotificationManager.emitNotification(message.text, message.username, this.name, message.timestamp);
            }
        }
    }

    //push message to messages array
    this.messages.push(m);
    this.$rootScope.$broadcast('scrollMessages');

    if(!this.active){
        this.unread++;
    }
};

Channel.prototype.parseMessageForAttachments = function(message){
    var text = message.text;
    message.attachments = [];
    var redmine_matches;
    if(this.active_parsers.indexOf('redmine') >= 0){
        /* redmine matches */
        var redmine_rx = /(T|t)(icket)(\s+)(#)(\d+)(\s+)?/g;
        redmine_matches = text.match(redmine_rx);
        if(redmine_matches !== null){
            for(var j = 0; j < redmine_matches.length; j++){
                var m = redmine_matches[j];
                var link = "http://redmine.webstaurantstore.com/issues/" + m.substr(m.indexOf("#") + 1).trim();
                message.attachments.push({ type: 'plain_link', link: link });
            }
        }
    }

    //'peg' is for catching .jpeg
    var image_extensions = ['png', 'jpg', 'gif', 'peg'];

    /* plain links & image links */
    var url_rx = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
    var url_matches = text.match(url_rx);
    if(url_matches !== null){
        for(var i = 0; i < url_matches.length; i++){
            if(redmine_matches === null || redmine_matches.indexOf(url_matches[i]) < 0){
                if(image_extensions.indexOf(url_matches[i].substr(url_matches[i].length - 3)) >= 0){
                    if(this.active_parsers.indexOf('images') >= 0){
                        message.attachments.push({ type: 'image_link', link: url_matches[i] });
                    }
                }
                else{
                    if(this.active_parsers.indexOf('links') >= 0){
                        message.attachments.push({ type: 'plain_link', link: url_matches[i] });
                    }
                }
            }
        }
    }


    return message;
};

Channel.prototype.getUserSettings = function(){

};

Channel.prototype.getHtmlClass = function(){
    if(this.active){
        return "active-channel";
    }
    else if(this.joined){
        return "joined-channel";
    }

    return "inactive-channel";
};

Channel.prototype.saveToServer = function(){
    //@TODO
    this.socket.emit('*', { type: 'update-channel-settings', channel_name: this.name, active_parsers: this.active_parsers, document_id: this.document_id });
};