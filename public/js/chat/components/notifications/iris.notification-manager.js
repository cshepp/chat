
angular.module('iris')
.service('NotificationManager', ['$filter', function($filter){

    this.emitNotification = function(message, username, channel, timestamp){
        var n = new Notification(message, { icon: "", body: "@" + username + " in #" + channel + " at " + $filter('timestampFormat')(timestamp) });

        //close after 3 seconds
        n.onshow = function(){
            setTimeout(function(){
                n.close();
            }, 5000);
        };

        n.onclick = function(){
            window.focus();
            //ChannelManager.changeActiveChannel(channel);
        };
    };
}]);