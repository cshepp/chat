
angular.module('iris')
.controller('AdminSettingsCtrl', ['$scope', '$rootScope', 'socket', 'RemoteUserManager', function($scope, $rootScope, socket, RemoteUserManager){
    //@TODO!

    $scope.new_user_username = "";
    $scope.new_user_password = "";
    $scope.new_user_is_admin = false;

    $rootScope.$on('adminSettingsModalActive', function(){
        //$scope.users = RemoteUserManager.getUsers();
        $scope.users = [];
    });

    $scope.addUser = function(){
        socket.emit('*', { type: 'create-new-user', username: $scope.new_user_username, password: $scope.new_user_password, is_admin: $scope.new_user_is_admin });
        $scope.new_user_username = "";
        $scope.new_user_password = "";
        $scope.new_user_is_admin = "";
    };

    $scope.revert = function(){
        $scope.new_user_username = "";
        $scope.new_user_password = "";
        $scope.new_user_is_admin = "";
    };

    socket.on('create-user-confirmation', function(){
        alert("New user created!");
    });
}]);