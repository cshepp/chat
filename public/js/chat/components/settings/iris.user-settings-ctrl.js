
angular.module('iris')
.controller('UserSettingsCtrl', ['$scope', '$rootScope', 'LocalUserManager', 'ChannelManager', 'socket', function($scope, $rootScope, LocalUserManager, ChannelManager, socket){

    $scope.keywords = "";
    $scope.mentions = false;
    $scope.channels = [];
    $scope.image = "";

    $scope.old_password = "";
    $scope.new_password_1 = "";
    $scope.new_password_2 = "";

    $rootScope.$on('userSettingsModalActive', function(){
        //hack!
        $scope.loadDataFromUser();
    });

    $scope.revert = function(){
        $scope.loadDataFromUser();

        $scope.old_password = "";
        $scope.new_password_1 = "";
        $scope.new_password_2 = "";
    };

    $scope.save = function(){
        LocalUserManager.user.settings.keywords = $scope.keywords.split(",").map(function(s){ return s.trim(); });
        LocalUserManager.user.settings.mentions = $scope.mentions;

        var channels_with_alerts = [];
        for(var i = 0; i < $scope.channels.length; i++){
            var c = $scope.channels[i];
            if(c.show_notifications){
                channels_with_alerts.push(c.name);
            }
        }

        LocalUserManager.user.settings.channels_with_alerts = channels_with_alerts;
        LocalUserManager.user.image_path = $scope.image;

        if( $scope.old_password.length   > 0 &&
            $scope.new_password_1.length > 0 &&
            $scope.new_password_2.length > 0 &&
            $scope.new_password_1 == $scope.new_password_2){

            //@TODO: password change
        }

        LocalUserManager.user.saveToServer();
    };

    $scope.loadDataFromUser = function(){
        $scope.keywords = LocalUserManager.user.getKeywordsSetting();
        $scope.mentions = LocalUserManager.user.getMentionsSetting();
        $scope.image    = LocalUserManager.user.image_path;

        var channelList = ChannelManager.getChannels();
        var channels_with_alerts = LocalUserManager.user.getChannelsWithNotificationsSetting();
        $scope.channels = Object.keys(channelList).map(function(name){
            return {
                name: name,
                show_notifications: (channels_with_alerts.indexOf(name) >= 0)
            };
        });
    };
}]);