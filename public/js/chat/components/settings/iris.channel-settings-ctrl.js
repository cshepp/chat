
angular.module('iris')
.controller('ChannelSettingsCtrl', ['$scope', 'ChannelManager', '$rootScope', 'socket', 'LocalUserManager', function($scope, ChannelManager, $rootScope, socket, LocalUserManager){

    $scope.channel_name = "";
    $scope.active_parsers = "";

    $scope.user = LocalUserManager.user;

    $rootScope.$on('channelSettingsModalActive', function(){
        $scope.init();
    });

    $scope.init = function(){
        var chan = ChannelManager.getActiveChannel();
        $scope.channel_name = chan.name;
        $scope.active_parsers = chan.active_parsers.join(',');
    };

    $scope.reset = function(){
        $scope.init();
    };

    $scope.save = function(){
        var chan = ChannelManager.getActiveChannel();
        chan.active_parsers = $scope.active_parsers.split(",").map(function(s){ return s.trim(); });
        chan.saveToServer();
    };

    $scope.delete = function(){
        if(confirm("Are you sure you want to delete this channel?")){
            socket.emit('*', { type: 'delete-channel', channel_name: $scope.channel_name });
        }
    };

}]);