
angular.module('iris')
.controller('NewChannelModalCtrl', ['$scope', 'ChannelManager', 'socket', function($scope, ChannelManager, socket){
    
    $scope.name = "";
    $scope.parsers = "links, images, redmine";

    $scope.reset = function(){
        $scope.name = "";
        $scope.parsers = "links, images, redmine";
    };

    $scope.create = function(){
        socket.emit('*', { type: 'create-channel', channel_name: $scope.name, active_parsers: $scope.parsers.split(",").map(function(s){ return s.trim(); }) });
    };
}]);