
angular.module('iris', ['btford.socket-io'])
.controller('MainCtrl', ['$scope',
                         '$rootScope',
                         'socket',
                         'LocalUserManager',
                         'RemoteUserManager',
                         'ChannelManager',
function MainCtrl ($scope, $rootScope, socket, LocalUserManager, RemoteUserManager, channel_manager){

//initialize user values
    $scope.LocalUser = LocalUserManager;

//initialize channels and such
    channel_manager.init($scope.LocalUser.user);
    $scope.channels = channel_manager.getChannels();
    $scope.active_users = channel_manager.getActiveUsers();

//request notification permission
    Notification.requestPermission( function(status){
        console.log("Notification Permissions: ", status);
    });

//Channel-related methods
    $scope.getActiveChannel = function(){
        return channel_manager.getActiveChannel();
    };

    $scope.changeActiveChannel = function(name){
        channel_manager.changeActiveChannel(name);
    };

    $scope.leaveActiveChannel = function(){
        channel_manager.leaveActiveChannel();
    };

//Message-related methods
    $scope.message_input = "";

    $scope.getActiveChannelMessages = function(){
        return channel_manager.getActiveChannelMessages();
    };

    $scope.loadPreviousMessages = function(){
        channel_manager.loadPreviousMessages();
    };

    $scope.sendMessage = function(e){
        if(e.keyCode === 13){ //enter key
            var text = $scope.message_input;
            socket.emit('*', { type: "message", channel_name: $scope.getActiveChannel().name, message: { username: $scope.LocalUser.user.username, timestamp: Date.now(), text: text }} );
            $scope.message_input = "";
        }
    };

//Search-related methods
    $scope.searchMessages = function(e){
        if(e.keyCode === 13){
            var text = $scope.search_text;
            socket.emit('search-messages', { search_term: text });
        }
    };

    socket.on('search-results', function(data){
        $scope.search_results = data;
        $('#search-dropdown-toggle').dropdown('toggle');
    });

//Settings Modals events (hack)
    $scope.userSettingsModalActive = function(){
        $rootScope.$broadcast('userSettingsModalActive', {});
    };

    $scope.channelSettingsModalActive = function(){
        $rootScope.$broadcast('channelSettingsModalActive', {});
    };

    $scope.adminSettingsModalActive = function(){
        $rootScope.$broadcast('adminSettingsModalActive', {});
    };

//User related events
    $scope.changeStatus = function(new_status){
        $scope.LocalUser.user.updateUserStatus(new_status);
    };

//DEBUG
    socket.on('control-action', function(data){
        //we migrated away from control-action events, so if any come through, we want to know about it
        console.log(data);
    });

    $rootScope.$on('scrollMessages', function(){
        try{ $scope.$digest(); }catch(e){}
        objDiv = document.getElementById("message-pane");
        objDiv.scrollTop = objDiv.scrollHeight + 250;
    });

    window.socket = socket;
}])
.directive('channellistitem', function(){ //@TODO - new file!
    return {
        restrict: 'E',
        template: '# <strong>{{chan.name}}</strong><span class="pull-right label label-danger" ng-hide="chan.unread == 0">{{chan.unread}}</span>',
        scope: {
            chan: "="
        }
    };
})
.filter('timestampFormat', function(){ //@TODO - new file!
    return function(value){
        var date = new Date(value);
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var hour = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();

        var toTwoDigits = function(value){
            var v = value + "";
            if(v.length == 1){
                return "0" + v;
            }

            return v;
        };

        var isToday = function(day, month, year){
            var today = new Date(Date.now());
            return (day == today.getDate() && month == today.getMonth() + 1 && year == today.getFullYear());
        };

        var getTime = function(hour, minutes, seconds){
            return (hour % 12 === 0 ? 12: hour % 12) + ":" + toTwoDigits(minutes) + " " + (hour / 12 >= 1 ? "PM" : "AM");
        };

        if(isToday(day, month, year)){
            return getTime(hour, minutes, seconds);
        }
        else{
            return month + "/" + day + "/" + year + " " + getTime(hour, minutes, seconds);
        }
    };
});