
module.exports = function(){
    return {
        "webserver_port": 8080,
        "elasticsearch_host": "http://localhost:9200",
        "elasticsearch_index_prefix": "chat"
    };
};