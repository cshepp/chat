
var Channel = require('./server.channel.js');

function ChannelManager(clients, ds){
    var _self = this;
    this.clients = clients;
    this.ds = ds;
    this.channels = {};

    this.clients.on('create-channel'            , _self.onCreateChannel         , _self);
    this.clients.on('request-channel-settings'  , _self.onRequestChannelSettings, _self);
    this.clients.on('update-channel-settings'   , _self.onUpdateChannelSettings , _self);
    this.clients.on('request-channel-list'      , _self.onRequestChannelList    , _self);
    this.clients.on('delete-channel'            , _self.onDeleteChannel         , _self);

    //@TODO - broadcast join/left to other clients so we can maintain a user list
    this.clients.on('joined-channel'            , _self.onJoinedChannel         , _self);
    this.clients.on('left-channel'              , _self.onLeftChannel           , _self);

    this.init();
}

/*
    Fetches active channel data and creates channel objects
*/
ChannelManager.prototype.init = function(){
    var _self = this;

    var callback = function(response){

        if(response.hits.total > 0){
            for(var i = 0; i < response.hits.total; i++){
                var t = response.hits.hits[i]._source;
                t.document_id = response.hits.hits[i]._id;

                var channel = new Channel(_self.clients, _self.ds, t);
                _self.channels[channel.name] = channel;
            }
        }
        else{
            console.log("Error in ChannelManager->init()");
        }
    };

    this.ds.search(
        this.ds.STATIC_INDEX,
        "channel",
        {
            "query": {
                "filtered": {
                    "query": {
                        "match_all": {}
                    }
                }
            }
        },
        callback
    );
};

ChannelManager.prototype.onCreateChannel = function(e, client){

    var channel = new Channel(this.clients, this.ds, e);
    this.channels[channel.name].push(channel);

    var on_save = function(){
        this.clients.io.sockets.emit('new-channel', channel.getProps());
    };

    this.ds.index(
        this.ds.STATIC_INDEX,
        "channel",
        {
            "name": channel.name,
            "active_parsers": channel.active_parsers
        },
        on_save
    );
};

//@TODO
ChannelManager.prototype.onRequestChannelSettings = function(e, client){};

//@TODO
ChannelManager.prototype.onUpdateChannelSettings = function(e, client){};

ChannelManager.prototype.onRequestChannelList = function(e, client){
    var _self = this;

    Object.keys(_self.channels).map(function(key){
        console.log(key);
        client.socket.emit('new-channel', _self.channels[key].getProps());
    });
};

ChannelManager.prototype.onDeleteChannel = function(e, client){
    //@TODO - ES doesn't actually delete the channels for some reason :/
    var _self = this;
    var on_result = function(result){
        if(!result) return;

        console.log(result._indices['chat-static']._shards);
        delete _self.channels[e.channel_name];
        _self.clients.io.sockets.emit('channel-deleted', { channel_name: e.channel_name });
    };

    var body = {
        "query": {
            "term": { "name": e.channel_name }
        }
    };

    console.log(body);

    this.ds.deleteByQuery(
        this.ds.STATIC_INDEX,
        "channel",
        body,
        on_result
    );
};

ChannelManager.prototype.getAllChannelsProps = function(){
    var _self = this;

    var chans =  Object.keys(_self.channels).map(function(key){
        var channel = _self.channels[key];
        return {
            name: channel.name,
            active_parsers: channel.active_parsers,
            document_id: channel.document_id
        };
    });

    var channel_object = {};
    for(var i = 0; i < chans.length; i++){
        channel_object[chans[i].name] = chans[i];
    }
    console.log(channel_object);
    return channel_object;
};

//
module.exports = function(clients, ds){
    return new ChannelManager(clients, ds);
};