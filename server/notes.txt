/*

Notes:
    - Channel
        + name: could be used for io.sockets.in(name).emit(...)
        + clients: {}
        + 
    - User
        + username
        + 
    - Client
        + User
        + socket
        + settings?
    - Message

    - ClientManager
        + listen for new connections, create new Clients
        + listen for diconnects, delete Clients

    - Client


    - Convert everything but messages to http requests instead of websockets?
        + what about deleting channels?...it's nice to have that change pushed to users via the WS
    - Define and implement an API for bots
        + /api/message/
    - Define and implement custom client-side parsers
    - Define and implement custom server-side "mutators"
*/