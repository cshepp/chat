
var Client = require('./server.client.js');
var jwt = require('jsonwebtoken');

function ClientManager(io, ds){
    var _self = this;
    this.io = io;
    this.ds = ds;
    this.clients = [];

    this.subs = {};

    this.io.sockets.on('connection', function(socket){ _self.onConnection.apply(_self, [socket]); });
    this.io.sockets.on('disconnect', this.onDisconnect);
}

ClientManager.prototype.onConnection = function(socket){
    var _self = this;
    var c = new Client(_self.ds, socket, _self);
    _self.clients.push(c);
    _self.dispatch({ type: "connection" }, c);
};

ClientManager.prototype.onDisconnect = function(socket){
    //@TODO
    console.log("User disconnected. ID: ", socket.id);
};

//register a callback
ClientManager.prototype.on = function(event_name, callback, scope){
    if(Object.keys(this.subs).indexOf(event_name) === -1){
        this.subs[event_name] = [];
    }

    this.subs[event_name].push({callback: callback, scope: scope});
};

//call callbacks when events occur
ClientManager.prototype.dispatch = function(e, client){
    if(Object.keys(this.subs).indexOf(e.type) >= 0){
        this.subs[e.type].map(function(obj){
            obj.callback.apply(obj.scope, [e, client]);
        });
    }
    else{
        console.log("No listeners for event: ", e);
    }
};

//verify admin status
ClientManager.prototype.verifyAdmin = function(client, callback){
    var _s = this;

    jwt.verify(client.socket.request._query.token, "test", function(err, decoded){
        if(err === null){
            if(decoded.is_admin){
                callback(true);
                return;
            }
        }
        callback(false);
        return;
    });
};

ClientManager.prototype.getClientByUsername = function(username){
    for(var i = 0; i < this.clients.length; i++){
        if(this.clients[i].user.username == username){
            return this.clients[i];
        }
    }
};


module.exports = function(io, ds){
    return new ClientManager(io, ds);
};