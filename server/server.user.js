
function User(clients, ds, username, client){
    this.clients = clients;
    this.ds = ds;
    this.username = username;
    this.status = "online";
    this.client = client;
    this.image_path = "";

    this.init();
}

User.prototype.init = function(){
    var _self = this;

    var callback = function(user_data){
        if(user_data.hits.hits.length == 1){

            _self.settings = user_data.hits.hits[0]._source;
            _self.image_path = user_data.hits.hits[0]._source.image_path;
            _self.document_id = user_data.hits.hits[0]._id;

            var packet = _self.settings;
            packet.document_id = _self.document_id;

            _self.client.socket.emit('user-settings', packet);
            _self.client.client_manager.io.sockets.emit('new-active-user', { username: _self.username, status: 'online', image_path: _self.image_path });
        }
        else{
            console.log("Error onRegisterUser: ", user_data);
        }
    };

    _self.ds.search(
        this.ds.STATIC_INDEX,
        "user",
        {
            "query": {
                "filtered": {
                    "filter": {
                        "term": { "username": _self.username }
                    }
                }
            }
        },
        callback
    );
};

User.prototype.onUpdateUser = function(e){

};


//
module.exports = function(clients, ds, username, client){
    return new User(clients, ds, username, client);
};