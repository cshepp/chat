/**
 * @class Channel
 * 
 *
 */


var Channel = require('./server.channel.channel.js');

/*

*/
function ChannelManager(clients, ds){
    var _self = this;
    this.clients = clients;
    this.ds = ds;
    this.channels = {};

    this.clients.on('create-channel', _self.onCreateChannel, _self);
    this.clients.on('update-channel', _self.onUpdateChannel, _self);
    this.clients.on('delete-channel', _self.onDeleteChannel, _self);
    this.clients.on('joined-channel', _self.onJoinedChannel, _self);
    this.clients.on('left-channel'  , _self.onLeftChannel  , _self);
    this.clients.on('connection'    , _self.onConnection   , _self);
    this.init();
}

/**
 * @method init
 * @description get all channels from DS and create them!
 */
ChannelManager.prototype.init = function(){

    var _self = this;

    var on_result = function(response){
        if(response.hits.total > 0){
            for(var i = 0; i < response.hits.total; i++){
                var _id = response.hits.hits[i]._id;

                var channel = new Channel(_self.clients, _self.ds, _id);
                _self.channels[_id] = channel;
            }
        }
        else{
            console.log("Error in ChannelManager->init()");
        }
    };

    this.ds.search(
        this.ds.STATIC_INDEX,
        "channel",
        {
            "query": {
                "filtered": {
                    "query": {
                        "match_all": {}
                    }
                }
            },
            "size": "100"
        },
        on_result
    );
};

/*

*/
ChannelManager.prototype.onCreateChannel = function(e, client){

    var _self = this;

    if(Object.keys(this.channels).indexOf(e.channel_name) < 0){
        var channel = new Channel(this.clients, this.ds);
        channel.name = e.channel_name;
        channel.active_parsers = e.active_parsers;

        channel.save(function(id){
            _self.channels[id] = channel;
        });
    }
    else{
        client.socket.emit('ERROR', { message: "There's already a channel with that name!"});
    }
};

/*

*/
ChannelManager.prototype.onUpdateChannel = function(e, client){

    var body = e.body;
    var channel = this.channels[body._id];
    channel.active_parsers = body.active_parsers;
    channel.save();
};

/*

*/
ChannelManager.prototype.onDeleteChannel = function(e, client){

    var body = e.body;
    var channel = this.channels[body._id];
    channel.delete();
    delete this.channels[body._id];
};

/*

*/
ChannelManager.prototype.onJoinedChannel = function(e, client){

    var body = e.body;
    var channel = this.channels[body._id];
    channel.participants.push(client.username);
    channel.sync();
};

/*

*/
ChannelManager.prototype.onLeftChannel = function(e, client){

    var body = e.body;
    var channel = this.channels[body._id];
    channel.participants.splice(channel.participants.indexOf(client.username), 1);
    channel.sync();
};

/*

*/
ChannelManager.prototype.onConnection = function(e, client){
    //send channels
    console.log("sending channels");
    for(var _id in this.channels){
        var channel = this.channels[_id];
        channel.sync();
    }
};

module.exports = ChannelManager;