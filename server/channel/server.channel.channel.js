
var SyncedObject = require('../server.synced-object.js');

/*

*/
function Channel(clients, ds, _id){
    SyncedObject.call(this, clients, ds, 'channel', _id);
    this.participants = [];
    this.init();
}

Channel.prototype = Object.create(SyncedObject.prototype);
Channel.prototype.constructor = Channel;

/*

*/
Channel.prototype.prepareSyncedData = function(){

    return {
        name: this.name,
        active_parsers: this.active_parsers,
        participants: this.participants
    };
};

/*

*/
Channel.prototype.prepareSavedData = function(){

    return {
        name: this.name,
        active_parsers: this.active_parsers
    };
};

/*

*/
Channel.prototype.hydrate = function(result){
    var _source = result._source;
    this.name = _source.name;
    this.active_parsers = _source.active_parsers;
};

module.exports = Channel;