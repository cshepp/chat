
/*

*/
function SyncedObject(clients, ds, object_type, _id){
	var _self = this;
	this.object_type = object_type;
	this._id = _id || "";
	this.clients = clients;
	this.ds = ds;
}

/*

*/
SyncedObject.prototype.init = function(){
	var _self = this;

	if(this._id.length > 0){
		this.get(this._id, function(res){ _self.hydrate.apply(_self, [res]); });
	}
};

/*

*/
SyncedObject.prototype.prepareSyncedData = function(){
	console.error("You must override this object's prepareSyncedData method: ", this.object_type);
};

/*

*/
SyncedObject.prototype.prepareSavedData = function(){
	console.error("You must override this object's prepareSavedData method: ", this.object_type);
};

/*

*/
SyncedObject.prototype.hydrate = function(){
	console.error("You must override this object's hydrate method: ", this.object_type);
};

/*

*/
SyncedObject.prototype.sync = function(){
	this.clients.io.sockets.emit(this.object_type, this.prepareSyncedData());
};

/*

*/
SyncedObject.prototype.save = function(callback){
	var _self = this;

	var on_update = function(response){
		_self.sync();
	};

	var on_index = function(response){
		_self._id = response.hits.hits[0]._id;
		_self.sync();
		if(typeof callback != 'undefined'){
			callback(_self._id);
		}
	};

	var update = function(){
		var idx = _self.object_type == 'message' ? _self.ds.ALL_MESSAGE_INDEXES : _self.ds.STATIC_INDEX;
		var data = _self.prepareSavedData();
		if(typeof data == 'undefined') return;

		_self.ds.update(
			idx,
			_self.object_type,
			_self._id,
			data,
			on_update
		);
	};

	var index = function(){
		var data = _self.prepareSavedData();
		if(typeof data == 'undefined') return;

		_self.ds.index(
			_self.ds.getIndex(_self.object_type),
			_self.object_type,
			data,
			on_index
		);
	};

	var _ = (typeof this._id != 'undefined' && this._id.length > 0) ? update() : index();
};

/*

*/
SyncedObject.prototype.get = function(_id, callback){

	var on_result = function(result){
		callback(result);
	};

	var idx = this.object_type == 'message' ? this.ds.ALL_MESSAGE_INDEXES : this.ds.STATIC_INDEX;

	this.ds.get(
		idx,
		this.object_type,
		_id,
		on_result
	);
};

/*

*/
SyncedObject.prototype.delete = function(){

};

module.exports = SyncedObject;