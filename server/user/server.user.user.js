
var SyncedObject = require('../server.synced-object.js');

/*

*/
function User(clients, ds, _id){
    SyncedObject.call(this, clients, ds, 'user', _id);
    this.init();
}

User.prototype = Object.create(SyncedObject.prototype);
User.prototype.constructor = User;

/*

*/
User.prototype.preparePublicSyncedData = function(){

    return {
        username: this.username,
        image_path: this.image_path,
        status: this.status,
        _id: this._id
    };
};

/*

*/
User.prototype.preparePrivateSyncedData = function(){
    var data = this.preparePublicSyncedData();
    data.settings = this.settings;
    return data;
};

/*

*/
User.prototype.prepareSavedData = function(){

    return {
        username: this.username,
        image_path: this.image_path,
        settings: this.settings
    };
};

/*

*/
User.prototype.hydrate = function(result){
    var _source     = result._source;
    this.username   = _source.username;
    this.image_path = _source.image_path;
    this.settings   = _source.settings;
    this.status     = "online";

    this.sync();
};

/*
    Override the sync method so that we can send sensitive info ONLY to the user associated with it.
    Other clients will get a bit less info.
*/
User.prototype.sync = function(){

    var client = this.getClientByUserId();
    if(typeof client != 'undefined'){
        console.log("sending private data to:", client.socket.id);
        client.socket.emit(this.object_type, this.preparePrivateSyncedData());
    }

    this.clients.io.sockets.emit(this.object_type, this.preparePublicSyncedData());
};

User.prototype.getClientByUserId = function(){

    var _id = this._id;
    for(var i = 0; i < this.clients.clients.length; i++){

        if(this.clients.clients[i].user_id == _id){
            return this.clients.clients[i];
        }
    }

    return undefined;
};

module.exports = User;