
var User = require('./server.user.user.js');
var password = require('password-hash-and-salt');

/*

*/
function UserManager(clients, ds, auth){
    var _self = this;
    this.clients = clients;
    this.ds = ds;
    this.auth = auth;
    this.users = {};

    this.clients.on('create-user'       , _self.onCreateUser        , _self);
    this.clients.on('update-user'       , _self.onUpdateUser        , _self);
    this.clients.on('delete-user'       , _self.onDeleteUser        , _self);
    this.clients.on('update-user-status', _self.onUpdateUserStatus  , _self);

    this.clients.on('connection'        , _self.onConnection        , _self);
    this.clients.on('disconnect'        , _self.onDisconnect        , _self);
}

/*

*/
UserManager.prototype.init = function(){
    //??
};

/*

*/
UserManager.prototype.onCreateUser = function(e, client){

    var _self = this;
    var body = e.body;
    var user = new User(_self.clients, _self.ds);
    user.username = body.username;
    //@TODO user.password = "";
    user.image_path = body.image_path;
    user.settings = body.settings;
    user.save(function(id){
        user._id = id;
        _self.users[id] = user;
    });
};

/*

*/
UserManager.prototype.onUpdateUser = function(e, client){

    var body = e.body;
    var user = this.users[body._id];
    user.image_path = body.image_path;
    user.settings = body.settings;
    user.save(function(){}, client);
};

/*

*/
UserManager.prototype.onDeleteUser = function(e, client){

    var body = e.body;
    var user = this.users[body._id];
    user.delete();
    delete this.users[body._id];
};

/*

*/
UserManager.prototype.onUpdateUserStatus = function(e, client){

    var body = e.body;
    var user = this.users[body._id];
    user.status = body.status;
    user.sync();
};

/*

*/
UserManager.prototype.onConnection = function(e, client){
    console.log("connection");
    var _self = this;
    this.auth.decodeJwt(client.socket.request._query.token, function(decoded){
        //register user
        client.user_id = decoded._id;

        if(_self.users.hasOwnProperty(decoded._id)){
            delete _self.users[decoded._id];
        }

        var user = new User(_self.clients, _self.ds, decoded._id);
        _self.users[decoded._id] = user;
    });
};

/*

*/
UserManager.prototype.onDisconnect = function(e, client){
    console.log("disconnect");
    delete this.users[client.user_id];
};

module.exports = UserManager;