
function Datastore(es, config){
    this.es = es;
    this.config = config;
    this.INDEX_PREFIX = config.elasticsearch_index_prefix;
    this.STATIC_INDEX = this.INDEX_PREFIX + "-static";
    this.ALL_MESSAGE_INDEXES = this.INDEX_PREFIX + '-messages*';
}

Datastore.prototype.getCurrentMessageIndex = function(){
    var today = new Date(Date.now());
    var month = (((today.getMonth() + 1) + "").length == 1 ? "0" + (today.getMonth() + 1) + "" : (today.getMonth() + 1) + "" );
    var day = ((today.getDate() + "").length == 1 ? "0" + today.getDate() : today.getDate() );
    return this.INDEX_PREFIX + "-messages-" + today.getFullYear() + "." + month + "." + day;
};

Datastore.prototype.getIndex = function(type){
    return type == 'message' ? this.getCurrentMessageIndex() : this.STATIC_INDEX;
};

Datastore.prototype.index = function(index_name, type, body, callback){
    this.es.index({
        index: index_name,
        type: type,
        body: body
    },
    function(err, response){
        if(err) console.log(err);
        if(response) callback(response);
    });
};

Datastore.prototype.get = function(index_name, type, _id, callback){
    this.es.get({
        index: index_name,
        type: type,
        id: _id
    },
    function(err, response){
        if(err){
            console.log(err);
            callback(false);
        }
        else{
            callback(response);
        }
    });
};

Datastore.prototype.search = function(index_name, type, body, callback){
    this.es.search({
        index: index_name,
        type: type,
        body: body
    },
    function(err, response){
        if(err){
            console.log(err);
            callback(false);
        }

        callback(response);
    });
};

Datastore.prototype.deleteByQuery = function(index_name, type, body, callback){
    this.es.deleteByQuery({
        index: index_name,
        type: type,
        body: body
    },
    function(err, response){
        if(err){
            console.log(err);
            callback(false);
        }

        if(response) callback(response);
    });
};

Datastore.prototype.update = function(index_name, type, id, body, callback){
    this.es.update({
        index: index_name,
        type: type,
        id: id,
        body: body
    },
    function(err, response){
        if(err){
            console.log(err);
            callback(false);
        }
        else{
            callback(response);
        }
    });
};


//
module.exports = function(es, config){
    return new Datastore(es, config);
};