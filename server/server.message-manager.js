
function MessageManager(clients, ds){
    var _self = this;
    this.clients = clients;
    this.ds = ds;

    this.clients.on('message', _self.onMessage, _self);
}

MessageManager.prototype.onMessage = function(e, client){
    if(e.channel_name.indexOf("user:") === 0){
        var user_name = e.channel_name.substring(5);
        var c = this.clients.getClientByUsername(user_name);
        c.socket.emit('message', e);        //send to recipient
        client.socket.emit('message', e);   //send back to sender
    }
    else{
        this.clients.io.to(e.channel_name).emit('message', e);
    }

    var on_save = function(result){
        //nothing to do here!
        console.log(result);
    };

    this.ds.index(
        this.ds.getCurrentMessageIndex(),
        'message',
        {
            "username": e.message.username,
            "text": e.message.text,
            "channel": e.channel_name,
            "timestamp": e.message.timestamp
        },
        on_save
    );
};


module.exports = function(clients, ds){
    return new MessageManager(clients, ds);
};