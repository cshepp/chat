
var Client = require('./server.client.client.js');
var jwt = require('jsonwebtoken');

function ClientManager(io, ds){
    var _self = this;
    this.io = io;
    this.ds = ds;
    this.clients = [];

    this.subs = {};

    this.io.sockets.on('connection', function(socket){ _self.onConnection.apply(_self, [socket]); });
    //this.io.sockets.on('disconnect', function(socket){ _self.onDisconnect.apply(_self, [socket]); });
}

ClientManager.prototype.onConnection = function(socket){
    var _self = this;
    console.log("new connected socket id:", socket.id);
    socket.on('disconnect', function(){
        _self.onDisconnect.apply(_self, [socket]);
    });

    var c = new Client(_self.ds, socket, _self);
    _self.clients.push(c);
    _self.dispatch({ type: "connection" }, c);
};

ClientManager.prototype.onDisconnect = function(socket){
    //@TODO - delete client from array
    var client = this.getClientBySocketId(socket.id);
    this.dispatch({ type: "disconnect" }, client);
    this.clients.splice(this.clients.indexOf(client), 1);
};

//register a callback
ClientManager.prototype.on = function(event_name, callback, scope){
    if(Object.keys(this.subs).indexOf(event_name) === -1){
        this.subs[event_name] = [];
    }

    this.subs[event_name].push({callback: callback, scope: scope});
};

//call callbacks when events occur
ClientManager.prototype.dispatch = function(e, client){
    if(Object.keys(this.subs).indexOf(e.type) >= 0){
        this.subs[e.type].map(function(obj){

            obj.callback.apply(obj.scope, [e, client]);
        });
    }
    else{
        console.log("No listeners for event: ", e);
    }
};

//verify admin status
ClientManager.prototype.verifyAdmin = function(client, callback){
    var _s = this;

    jwt.verify(client.socket.request._query.token, "test", function(err, decoded){
        if(err === null){
            if(decoded.is_admin){
                callback(true);
                return;
            }
        }
        callback(false);
        return;
    });
};

ClientManager.prototype.getClientByUsername = function(username){
    for(var i = 0; i < this.clients.length; i++){
        if(this.clients[i].user.username == username){
            return this.clients[i];
        }
    }
};

ClientManager.prototype.getClientBySocketId = function(socket_id){
    for(var i = 0; i < this.clients.length; i++){
        if(this.clients[i].id == socket_id){
            return this.clients[i];
        }
    }

    return undefined;
};


module.exports = ClientManager;