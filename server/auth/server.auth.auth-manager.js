
var password = require('password-hash-and-salt');
var jwt = require('jsonwebtoken');

function AuthManager(ds){
    this.ds = ds;
}

AuthManager.prototype.authenticateLogin = function(username, pw, callback){
    var on_result = function(response){
        if(response.hits.total == 1){
            var stored_password = response.hits.hits[0]._source.password;
            var username = response.hits.hits[0]._source.username;
            var settings = response.hits.hits[0]._source.settings;
            var _id = response.hits.hits[0]._id;
            password(pw).verifyAgainst(stored_password, function(error, verified){
                if(verified){
                    callback(true, {
                        username: username,
                        settings: settings,
                        _id: _id
                    });
                    return;
                }
                else{
                    if(error) console.log(error);
                    callback(false, {});
                    return;
                }
            });
        }
        else{
            callback(false);
        }
    };

    this.ds.search(
        this.ds.STATIC_INDEX,
        "user",
        {
            "query": {
                "filtered": {
                    "query": {
                        "match_all": {}
                    },
                    "filter": {
                        "bool": {
                            "must": {
                                "term": { "username": username }
                            }
                        }
                    }
                }
            }
        }
        , on_result
    );
};

AuthManager.prototype.isAdmin = function(client){

};

AuthManager.prototype.decodeJwt = function(token, callback){
    jwt.verify(token, "test", function(err, decoded){
        if(err === null){
            callback(decoded);
        }
        else{
            callback(false);
        }
    });
};

module.exports = AuthManager;