
var password = require('password-hash-and-salt');
var User = require('./server.user.js');

function UserManager(clients, ds){
    var _self = this;
    this.clients = clients;
    this.ds = ds;
    this.users = [];

    this.clients.on('create-new-user'       , _self.onCreateNewUser         , _self);
    this.clients.on('request-all-user-data' , _self.onRequestAllUserData    , _self);
    this.clients.on('register-user'         , _self.onRegisterUser          , _self);
    this.clients.on('update-user'           , _self.onUpdateUser            , _self);
    this.clients.on('update-user-status'    , _self.onUpdateUserStatus      , _self);
}

UserManager.prototype.onCreateNewUser = function(e, client){
    var _self = this;

    _self.clients.verifyAdmin(client, function(success){
        if(success){

            var on_save = function(res){
                client.socket.emit("create-user-confirmation", { success: true });
            };

            var on_hash = function(err, hash){
                _self.ds.index(
                    _self.ds.STATIC_INDEX,
                    "user",
                    {
                        "username": e.username,
                        "password": hash,
                        "image_path": "",
                        "settings": {
                            "percolater_triggers": [""],
                            "channels_with_alerts": [""],
                            "auto_join_channels": ["general"],
                            "keywords": [],
                            "mentions": false,
                            "is_admin": e.is_admin
                        }
                    },
                    on_save
                );
            };

            password(e.password).hash(on_hash);
        }
    });
};

UserManager.prototype.onRequestAllUserData = function(e, client){
    var on_result = function(res){
        var users = res.hits.hits.map(function(h){
            var user = h._source;
            user.password = ""; //blank out the pw
            user.document_id = h._id;
            return user;
        });

        client.socket.emit('all-user-data', { users: users });
    };

    this.ds.search(
        this.ds.STATIC_INDEX,
        "user",
        {
            "query": {
                "filtered": {
                    "query": {
                        "match_all": {}
                    }
                }
            },
            "size": 1000
        },
        on_result
    );
};

UserManager.prototype.onRegisterUser = function(e, client){
    var user = new User(this.client_manager, this.ds, e.username, client);
    this.users.push(user);
    client.user = user;

    //send a list of active users
    for(var i =0; i < this.users.length; i++){
        client.socket.emit('new-active-user', { username: this.users[i].username, status: this.users[i].status, image_path: this.users[i].image_path });
    }
};

UserManager.prototype.onUpdateUser = function(e, client){

    on_update = function(response){
        //?
    };

    this.ds.update(
        this.ds.STATIC_INDEX,
        "user",
        e.document_id,
        {
            "doc": {
                "settings": {
                    "mentions": e.mentions,
                    "keywords": e.keywords,
                    "channels_with_alerts": e.channels_with_alerts
                },
                "image_path": e.image_path
            }
        },
        on_update
    );
};

UserManager.prototype.onUpdateUserStatus = function(e, client){
    client.user.status = e.status;
    this.clients.io.sockets.emit('updated-user-status', { username: e.username, status: e.status, image_path: client.user.image_path });
};

UserManager.prototype.getUserByClient = function(client){

};

UserManager.prototype.authenticate = function(username, pw, callback){
    var on_result = function(response){
        if(response.hits.total == 1){
            var stored_password = response.hits.hits[0]._source.password;
            var username = response.hits.hits[0]._source.username;
            var settings = response.hits.hits[0]._source.settings;
            password(pw).verifyAgainst(stored_password, function(error, verified){
                if(verified){
                    callback(true, {
                        username: username,
                        settings: settings
                    });
                    return;
                }
                else{
                    if(error) console.log(error);
                    callback(false, {});
                    return;
                }
            });
        }
        else{
            callback(false);
        }
    };

    this.ds.search(
        this.ds.STATIC_INDEX,
        "user",
        {
            "query": {
                "filtered": {
                    "query": {
                        "match_all": {}
                    },
                    "filter": {
                        "bool": {
                            "must": {
                                "term": { "username": username }
                            }
                        }
                    }
                }
            }
        }
        , on_result
    );
};


module.exports = function(clients, ds){
    return new UserManager(clients, ds);
};

