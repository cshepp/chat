
function Channel(clients, ds, settings){
    var _self = this;
    this.clients = clients;
    this.ds = ds;
    this.name = settings.name;
    this.active_parsers = settings.active_parsers;
    this.document_id = settings.document_id || null;

    this.clients.on('get-previous-messages' , _self.onGetPreviousMessages   , _self);
    this.clients.on('join-channel'          , _self.onJoinChannel           , _self);
    this.clients.on('leave-channel'         , _self.onLeaveChannel          , _self);
}

Channel.prototype.onGetPreviousMessages = function(e, client){
    //check channel name!
};

Channel.prototype.onJoinChannel = function(e, client){
    if(e.channel_name == this.name){
        this.clients.io.to(this.name).emit('user-joined-channel', { channel_name: e.channel_name, username: client.user.username });
    }
};

Channel.prototype.onLeaveChannel = function(e, client){
    if(e.channel_name == this.name){
        this.clients.io.to(this.name).emit('user-left-channel', { channel_name: e.channel_name, username: client.user.username });
        client.socket.emit('user-left-channel', { channel_name: e.channel_name, username: client.user.username });
    }
};

Channel.prototype.getProps = function(){
    return {
        name: this.name,
        active_parsers: this.active_parsers
    };
};




//
module.exports = function(clients, ds, settings){
    return new Channel(clients, ds, settings);
};