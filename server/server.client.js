
function Client(ds, socket, client_manager){
    var _self = this;
    this.ds = ds;
    this.socket = socket;
    this.id = this.socket.id;
    this.client_manager = client_manager;
    this.user = null;

    //direct all socket message to the client manager
    this.socket.on('*', function(socket){ _self.onGlobalAction.apply(_self, [socket]); });
    this.socket.on('local', function(socket){ _self.onLocalAction.apply(_self, [socket]); });
}

Client.prototype.onGlobalAction = function(e){
    this.client_manager.dispatch(e, this);
};

Client.prototype.onLocalAction = function(e){
    switch(e.type){
        case "join-channel":
            this.onJoinChannel(e);
            break;
        case "leave-channel":
            this.onLeaveChannel(e);
            break;
        default:
            console.log("No local handler for: " + e.type);
    }
};

Client.prototype.onJoinChannel = function(e){
    var _self = this;
    //join the channel at the socket level
    this.socket.join(e.channel_name);

    //automattically send client a message backfill
    var on_result = function(result){
        var messages = result.hits.hits.map(function(r){ return r._source; });
        console.log("Backfilling... ");
        _self.socket.emit('message-backfill', { channel_name: e.channel_name, messages: messages });
    };

    this.ds.search(
        this.ds.ALL_MESSAGE_INDEXES,
        'message',
        {
            "query": {
                "filtered": {
                    "filter": {
                        "term": {
                            "channel": e.channel_name
                        }
                    }
                }
            },
            "sort": {
                "timestamp": { "order": "desc" }
            }
        }
        , on_result
    );

    //tell channel manager to tell others that this client has joined the channel
    this.client_manager.dispatch(e, this);
};

Client.prototype.onLeaveChannel = function(e){
    //leave channel
    this.socket.leave(e.channel_name);
    this.client_manager.dispatch(e, this);
};

//
module.exports = function(ds, socket, client_manager){
    return new Client(ds, socket, client_manager);
};