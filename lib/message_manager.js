/* message_manager */

var CreateClient = require('./client.js');
var jwt = require('jsonwebtoken');
var CreateChannel = require('./channel.js');

module.exports = function(io, dsm){
    return new MessageManager(io, dsm);
};

function MessageManager(io, dsm){
    var _self = this;
    this.dsm = dsm;

    this.io = io;       //reference to the main socket.io connection
    this.clients = {};  //a hashmap of socket IDs to socket objects for our connected clients
    this.channels = {}; //a hashmap of channel names to channel objects

    //event handlers
    io.sockets.on('connection', function(socket){
        onConnection.apply(_self, [socket]);

        console.log(socket);

        //basic messages
        socket.on('message', function(data){
            onMessage.apply(_self, [socket, data]);
        });

        //control actions - DEPRECATED
        socket.on('control-action', function(data){
            onControlAction.apply(_self, [socket, data]);
        });

        socket.on('create-channel', function(data){
            createChannel.apply(_self, [socket, data]);
        });

        socket.on('join-channel', function(data){
            joinChannel.apply(_self, [socket, data]);
        });

        socket.on('leave-channel', function(data){
            leaveChannel.apply(_self, [socket, data]);
        });

        socket.on('change-user-settings', function(data){
            changeUserSettings.apply(_self, [socket, data]);
        });

        socket.on('register-user', function(data){
            registerUser.apply(_self, [socket, data]);
        });

        socket.on('create-new-user', function(data){
            createUser.apply(_self, [socket, data]);
        });

        socket.on('get-previous-messages', function(data){
            getPreviousMessages.apply(_self, [socket, data]);
        });

        socket.on('request-channel-settings', function(data){
            sendChannelSettings.apply(_self, [socket, data]);
        });

        socket.on('update-channel-settings', function(data){
            updateChannelSettings.apply(_self, [socket, data]);
        });

        socket.on('request-channel-list', function(data){
            sendChannelList.apply(_self, [socket, data]);
        });

        socket.on('delete-channel', function(data){
            deleteChannel.apply(_self, [socket, data]);
        });

        socket.on('update-user', function(data){
            updateUser.apply(_self, [socket, data]);
        });

        socket.on('request-all-user-data', function(data){
            sendUserData.apply(_self, [socket, data]);
        });

        socket.on('search-messages', function(data){
            searchMessages.apply(_self, [socket, data]);
        });

        socket.on('disconnect', function(data){
            console.log('client left...');
            console.log(Object.keys(_self.clients).length + " clients");
            io.sockets.emit('client-inactive', { username: _self.clients[socket.id].username });
            delete _self.clients[socket.id];
        });
    });

    //private methods
    var onConnection = function(socket){
        this.clients[socket.id] = CreateClient(socket);
        console.log("new client...");
        console.log(Object.keys(this.clients).length + " clients");
    };

    var onMessage = function(socket, data){
        var client = this.clients[socket.id];
        var _s = this;

        jwt.verify(socket.request._query.token, "test", function(err, decoded){
            if(err === null){
                //success
                if(decoded.username == data.message.username){ //verify username
                    if(Object.keys(_s.channels).indexOf(data.channel_name) < 0){
                        //createChannel.apply(_s, [socket, data]);
                        joinChannel.apply(_s, [socket, data]);
                    }

                    console.log("emitting message to " + data.channel_name);
                    io.to(data.channel_name).emit('message', data);
                    //send message to ES
                    _s.dsm.indexMessage(data);
                }
            }
            else{
                console.log('jwt verification error');
            }
        });
    };

    var onControlAction = function(socket, data){
        console.log("!!!!!!!!! --- still sending a control-action", data);
    };

    var createChannel = function(socket, data){
        if(Object.keys(this.channels).indexOf(data.channel_name) == -1){
            var c = CreateChannel(data.channel_name, data.active_parsers);
            this.channels[data.channel_name] = c;
            this.dsm.createChannel(c);
            this.io.sockets.emit('new-channel', c);
        }
    };

    var joinChannel = function(socket, data){
        if(Object.keys(this.channels).indexOf(data.channel_name) >= 0){
            var client = this.clients[socket.id];

            //join the room
            socket.join(data.channel_name);

            //send recent messages
            this.dsm.getBackfillMessagesByChannel(data.channel_name, function(messages){
                socket.emit('message-backfill', { channel_name: data.channel_name, messages: messages });
            });

            //send notification to others in this room
            this.io.to(data.channel_name).emit('user-joined-channel', { channel_name: data.channel_name, username: client.username });
        }
    };

    var leaveChannel = function(socket, data){
        if(Object.keys(this.channels).indexOf(data.channel_name) >= 0){
            var client = this.clients[socket.id];
            socket.leave(data.channel_name);
            this.io.to(data.channel_name).emit('user-left-channel', { channel_name: data.channel_name, username: client.username });
            socket.emit('user-left-channel', { username: client.username, channel_name: data.channel_name });
        }
    };

    var deleteChannel = function(socket, data){
        if(this.channels.hasOwnProperty(data.channel_name)){
            delete this.channels[data.channel_name];
            this.dsm.deleteChannel(data.channel_name);
            this.io.sockets.emit('channel-deleted', { channel_name: data.channel_name });
        }
    };

    var registerUser = function(socket, data){
        this.clients[socket.id].username = data.username;
        //send this new user to all connected sockets
        io.sockets.emit('client-active', { username: data.username });

        //send the new user some basic settings
        this.dsm.getUserSettings(data, function(settings){
            socket.emit('user-settings', settings);
        });

        //send list of clients that are active to the new user that joined
        Object.keys(_self.clients).map(function(key){
            var c = _self.clients[key];
            socket.emit('client-active', { username: c.username });
        });
    };

    var createUser = function(socket, data){
        var _s = this;

        jwt.verify(socket.request._query.token, "test", function(err, decoded){
            if(err === null){
                if(decoded.is_admin){
                    _s.dsm.createUser(data, function(success){
                        socket.emit("create-user-confirmation", { success: success });
                    });
                }
            }
            else{
                console.log('jwt verification error');
            }
        });
    };

    var getPreviousMessages = function(socket, data){
        this.dsm.getPreviousMessages(data, function(messages){
            socket.emit('previous-messages', { channel_name: data.channel_name, messages: messages });
        });
    };

    var sendChannelSettings = function(socket, data){
        this.dsm.getGlobalChannelSettings(data, function(settings){
            socket.emit('update-channel-settings', { channel_name: data.channel_name });
        });
    };

    var updateChannelSettings = function(socket, data){
        this.dsm.updateChannel(data);
    };

    var sendChannelList = function(socket, data){
        Object.keys(_self.channels).map(function(chan){
            socket.emit('new-channel', _self.channels[chan]);
        });
    };

    var updateUser = function(socket, data){
        this.dsm.updateUser(data);
    };

    var sendUserData = function(socket, data){
        //@TODO - verify the socket is_admin
        this.dsm.getAllUserData(function(users){
            socket.emit('all-user-data', { users: users });
        });
    };

    var searchMessages = function(socket, data){
        this.dsm.searchMessages(data, function(data){
            socket.emit('search-results', data);
        });
    };
}

MessageManager.prototype.initChannels = function(c){
    for(var i = 0; i < c.length; i++){
        this.channels[c[i].name] = c[i];
    }
};