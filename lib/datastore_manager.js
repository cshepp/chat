/* Data Store Manager */

var password = require('password-hash-and-salt');

module.exports = function(esClient, config){
    return new DatastoreManager(esClient, config);
};

function DatastoreManager(esClient, config){
    this.client = esClient;
    this.prefix = config.elasticsearch_index_prefix;
    this.static_index = this.prefix + "-static";

    this.getCurrentMessageIndex = function(){
        var today = new Date(Date.now());
        var month = (((today.getMonth() + 1) + "").length == 1 ? "0" + (today.getMonth() + 1) + "" : (today.getMonth() + 1) + "" );
        var day = ((today.getDate() + "").length == 1 ? "0" + today.getDate() : today.getDate() );
        return this.prefix + "-messages-" + today.getFullYear() + "." + month + "." + day;
    };

    this.userExists = function(username, pass, callback){
        this.client.search({
            "index": this.static_index,
            "type": "user",
            "body": {
                "query": {
                    "filtered": {
                        "query": {
                            "match_all": {}
                        },
                        "filter": {
                            "bool": {
                                "must": {
                                    "term": { "username": username }
                                }
                            }
                        }
                    }
                }
            }
        },
        function(err, response){
            if(!err && response.hits.total == 1){
                var stored_password = response.hits.hits[0]._source.password;
                var username = response.hits.hits[0]._source.username;
                var settings = response.hits.hits[0]._source.settings;
                password(pass).verifyAgainst(stored_password, function(error, verified){
                    if(verified){
                        callback(true, {
                            username: username,
                            settings: settings
                        });
                    }
                    else{
                        if(error) console.log(error);
                        callback(false, {});
                    }
                });
            }
            else{
                callback(false, {});
            }
        });
    };


    this.getChannels = function(callback){
        this.client.search({
            "index": this.static_index,
            "type": "channel",
            "body": {
                "query": {
                    "filtered": {
                        "query": {
                            "match_all": {}
                        }
                    }
                }
            }
        },
        function(err, response){
            var channels = [];
            if(!err && response.hits.total > 0){
                for(var i = 0; i < response.hits.total; i++){
                    var c = response.hits.hits[i]._source;
                    c.document_id = response.hits.hits[i]._id;
                    channels.push(c);
                }
            }

            callback(channels);
        });
    };

    this.createChannel = function(channel){
        var _self = this;
        _self.client.index({
            "index": _self.static_index,
            "type": "channel",
            "body": {
                "name": channel.name,
                "active_parsers": channel.active_parsers
            }
        }, function(err, response){
            if(err) console.log(err);
        });
    };

    this.createUser = function(data, callback){
        var _self = this;

        password(data.password).hash(function(err, hash){
            _self.client.index({
                "index": _self.static_index,
                "type": "user",
                "body": {
                    "username": data.username,
                    "password": hash,
                    "image_path": "",
                    "settings": {
                        "percolater_triggers": [""],
                        "channels_with_alerts": [""],
                        "auto_join_channels": ["general"],
                        "keywords": [],
                        "mentions": false,
                        "is_admin": data.is_admin
                    }
                }
            },
            function(err, response){
                callback(!err);
            });
        });
    };

    this.indexMessage = function(data){
        var _self = this;
        _self.client.index({
            "index": _self.getCurrentMessageIndex(),
            "type": "message",
            "body": {
                "username": data.message.username,
                "text": data.message.text,
                "channel": data.channel_name,
                "timestamp": data.message.timestamp
            }
        },
        function(err, response){
            if(err) console.log(err);
        });
    };

    this.getBackfillMessagesByChannel = function(channel_name, callback){
        var _self = this;
        _self.client.search({
            "index": _self.prefix + "-messages*",
            "type": "message",
            "body": {
                "query": {
                    "filtered": {
                        "filter": {
                            "term": {
                                "channel": channel_name
                            }
                        }
                    }
                },
                "sort": {
                    "timestamp": { "order": "desc" }
                }
            }
        },
        function(err, response){
            if(!err){
                callback(response.hits.hits.map(function(res){
                    return res._source;
                }));
            }

            if(err) console.log(err);
        });
    };

    this.searchMessages = function(data, callback){
        var _self = this;
        _self.client.search({
            "index": _self.prefix + "-messages*",
            "type": "message",
            "body": {
                "query": {
                    "match": {
                        "text": data.search_term
                    }
                }
            }
        },
        function(err, response){
            if(!err){
                callback(response.hits.hits.map(function(res){
                    return res._source;
                }));
            }

            if(err) console.log(err);
        });
    };

    this.getPreviousMessages = function(data, callback){
        var _self = this;
        _self.client.search({
            "index": _self.prefix + "-messages*",
            "type": "message",
            "body":{
                "query": {
                    "filtered": {
                        "filter": {
                            "bool": {
                                "must": [
                                    { "term": { "channel": data.channel_name }},
                                    { "range": { "timestamp": { "lt": data.from }}}
                                ]
                            }
                        }
                    }
                },
                "sort": {
                    "timestamp": { "order": "desc" }
                }
            }
        },
        function(err, response){
            if(!err){
                callback(response.hits.hits.map(function(res){
                    return res._source;
                }));
            }

            if(err) console.log(err);
        });
    };

    this.getGlobalChannelSettings = function(data, callback){
        var _self = this;
        //first we have to get the global channel settings
        _self.client.search({
            "index": _self.prefix + "-static",
            "type": "channel",
            "body": {
                "query": {
                    "filtered": {
                        "filter": {
                            "term": { "name": data.channel_name }
                        }
                    }
                }
            }
        }, function(err, response){
            if(!err && response.hits.hits.length == 1){
                callback(response.hits.hits[0]._source);
            }

            if(err) console.log(err);
        });
    };

    this.getUserSettings = function(data, callback){
        var _self = this;
        _self.client.search({
            "index": _self.prefix + "-static",
            "type": "user",
            "body": {
                "query": {
                    "filtered": {
                        "filter": {
                            "term": { "username": data.username }
                        }
                    }
                }
            }
        }, function(err, response){
            if(!err && response.hits.hits.length == 1){
                var u = response.hits.hits[0]._source
                u.document_id = response.hits.hits[0]._id;
                callback(u);
            }

            if(err) console.log(err);
        });
    };

    this.deleteChannel = function(name){
        var _self = this;
        _self.client.deleteByQuery({
            "index": _self.static_index,
            "type": "channel",
            "body": {
                "query": {
                    "term": { "name": name }
                }
            }
        }, function(err, response){
            if(err) console.log(err);
        });
    };

    this.updateChannel = function(data){
        var _self = this;
        _self.client.update({
            "index": _self.static_index,
            "type": "channel",
            "id": data.document_id,
            "body": {
                "doc": {
                    "name": data.name,
                    "active_parsers": data.active_parsers
                }
            }
        }, function(err, response){
            if(err) console.log(err);
        });
    };

    this.updateUser = function(data){
        var _self = this;
        _self.client.update({
            "index": _self.static_index,
            "type": "user",
            "id": data.document_id,
            "body":{
                "doc": {
                    "settings": {
                        "mentions": data.mentions,
                        "keywords": data.keywords,
                        "channels_with_alerts": data.channels_with_alerts
                    }
                }
            }
        }, function(err, response){
            if(err) console.log(err);
        });
    };

    this.getAllUserData = function(callback){
        var _self = this;
        _self.client.search({
            "index": _self.static_index,
            "type": "user",
            "body": {
                "query": {
                    "filtered": {
                        "query": {
                            "match_all": {}
                        }
                    }
                }
            }
        }, function(err, response){
            if(!err){
                callback(response.hits.hits.map(function(h){
                    var user = h._source;
                    user.password = ""; //blank out the pw
                    user.document_id = h._id;
                    return user;
                }));
            }

            if(err) console.log(err);
        });
    };
}