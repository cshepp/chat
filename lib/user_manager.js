/* User Manager */

var password = require('password-hash-and-salt');

module.exports = function(dsm){
    return new UserManager(dsm);
};

function UserManager(dsm){
    var _self = this;
    this.dsm = dsm;

    //private methods
    var hashPassword = function(str, callback){
        password(str).hash(function(err, hash){
            if(!err && hash){
                callback(hash);
            }
            else{
                console.log("UserManager Error: " + err);
            }
        });
    };

    var checkUserExists = function(username, password, callback){
        this.dsm.userExists(username, password, callback);
    };

    var updateUserSettings = function(){

    };

    //public methods
    this.authenticate = function(username, password, callback){
        checkUserExists.apply(_self, [username, password, callback]);
    };
}