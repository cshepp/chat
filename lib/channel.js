
module.exports = function(name, parsers){
    return new Channel(name, parsers);
};

function Channel(name, parsers){
    this.name = name;
    this.active_parsers = parsers;
    this.active_users = [];
}