/* client */

module.exports = function(socket){
    return new Client(socket);
};

function Client(socket){
    this.socket = socket;
    this.id = socket.id;
    this.username = "";
}