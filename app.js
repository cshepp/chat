/*
    main script for the chat server
*/

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var server = require('http').Server(app);
var io = require('socket.io')(server);
var socketio_jwt = require('socketio-jwt');
var jwt = require('jsonwebtoken');
var elasticsearch = require('elasticsearch');
var ESClient = new elasticsearch.Client({ host: "http://localhost:9200"});
var config = require('./config/config')();

var jwt_secret = "test";

//setup websocket auth
io.use(function(socket, next){
    jwt.verify(socket.request._query.token, jwt_secret, function(err, decoded){
        if(err === null){
            next();
        }
        else{
            console.log('jwt verification error');
            next(new Error('not authorized'));
        }
    });
});

var DatastoreManager = require('./lib/datastore_manager')(ESClient, config);
var MessageManager = require("./lib/message_manager")(io, DatastoreManager);
var UserManager = require("./lib/user_manager")(DatastoreManager);

//initialize channels
DatastoreManager.getChannels(function(channels){
    MessageManager.initChannels.apply(MessageManager, [channels]);
});



//-----all standard http requests are managed here-----\\

//setup static public folder for serving the frontend
app.use(express.static(__dirname + '/public'));

//setup body parsing middleware for POST requests
app.use(bodyParser.urlencoded({ extended: false }));

//setup authentication middleware - @TODO
/*app.all('/api/*', requireAuthentication);*/

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/public/index.html');
});

app.get('/chat', function(req, res){
    res.sendFile(__dirname + '/public/chat.html');
});

//handle login requests
app.post('/login', function(req, res){
    //verify credentials
    var body = req.body;
    UserManager.authenticate(body.username, body.password, function(exists, userdata){
        if(exists){
            var token = jwt.sign({ username: body.username, is_admin: userdata.settings.is_admin }, jwt_secret, { expiresInMinutes: 60*12 });
            if(token){
                res.json({ "success": true, "token": token, "userdata": userdata, "channels": MessageManager.channels });
            }
            else{
                res.json({ "success": false });
            }
        }
        else{
            res.json({ "success": false });
        }
    });
});

server.listen(config.webserver_port, function(){
    console.log("Chat server running on port: " + config.webserver_port);
});
