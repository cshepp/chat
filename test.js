
/*deps*/
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var server = require('http').Server(app);
var io = require('socket.io')(server);
var socketio_jwt = require('socketio-jwt');
var jwt = require('jsonwebtoken');
var elasticsearch = require('elasticsearch');
var ESClient = new elasticsearch.Client({ host: "http://localhost:9200"});
var config = require('./config/config')();

/*classes*/
var ns_ClientManager    = require('./server/client/server.client.client-manager.js');
var ns_ChannelManager   = require('./server/channel/server.channel.channel-manager.js');
var ns_UserManager      = require('./server/user/server.user.user-manager.js');
var ns_AuthManager      = require('./server/auth/server.auth.auth-manager.js');
//var ns_MessageManager   = require('./server/message/server.message.message-manager.js');

var jwt_secret = "test";

var Datastore       = require('./server/server.datastore.js')(ESClient, config);
var AuthManager     = new ns_AuthManager(Datastore);
var ClientManager   = new ns_ClientManager(io, Datastore);
var ChannelManager  = new ns_ChannelManager(ClientManager, Datastore);
var UserManager     = new ns_UserManager(ClientManager, Datastore, AuthManager);
//var MessageManager  = new ns_MessageManager(ClientManager, Datastore);


//-----all standard http requests are managed here-----\\

//setup static public folder for serving the frontend
app.use(express.static(__dirname + '/public'));

//setup body parsing middleware for POST requests
app.use(bodyParser.urlencoded({ extended: false }));

//setup authentication middleware - @TODO
/*app.all('/api/*', requireAuthentication);*/

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/public/index.html');
});

app.get('/chat', function(req, res){
    res.sendFile(__dirname + '/public/chat.html');
});

//handle login requests
app.post('/login', function(req, res){
    //verify credentials
    var body = req.body;
    AuthManager.authenticateLogin(body.username, body.password, function(exists, userdata){
        if(exists){
            var token = jwt.sign({ username: body.username, is_admin: userdata.settings.is_admin, _id: userdata._id }, jwt_secret, { expiresInMinutes: 60*12 });
            if(token){
                res.json({ "success": true, "token": token, "_id": userdata._id });
            }
            else{
                res.json({ "success": false });
            }
        }
        else{
            res.json({ "success": false });
        }
    });
});

server.listen(config.webserver_port, function(){
    console.log("Chat server running on port: " + config.webserver_port);
});
