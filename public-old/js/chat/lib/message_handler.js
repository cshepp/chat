
function MessageParser($scope){
    this.scope = $scope;
}

MessageParser.prototype.handleControlAction = function(data){

    switch(data.action_type){
        case "new-channel":
            var prefs = getChannelPreferences(data.channel_name);
            $scope.channels.push(prefs);
            var chans = JSON.parse(localStorage.channels);
            if(chans.indexOf(data.channel_name) < 0){
                chans.push(data.channel_name);
            }
            localStorage.setItem('channels', JSON.stringify(chans));
            break;
        case "user-joined-channel":
            if(data.username == $scope.user.username){
                //we joined this channel
                var chan = $scope.getChannelObjectByName(data.channel_name);
                chan.joined = true;
            }
            else{
                //@TODO - add user lists per channel? or just globally?
            }
            break;
        case "user-left-channel":

            break;
        case "create-user-confirmation":
            alert("User successfully created!");
            break;
        case "message-backfill":
            var ch = $scope.getChannelObjectByName(data.channel_name);
            if(ch){
                data.messages.map(function(m){
                    ch.messages.push($scope.parseMessage(data.channel_name, m));
                });
            }
            break;
        case "previous-messages":
            var c = $scope.getChannelObjectByName(data.channel_name);
            if(c){
                data.messages.map(function(m){
                    c.messages.push($scope.parseMessage(data.channel_name, m));
                });

                $scope.scrollMessages();
            }
            break;
        case "client-active":
            if(data.username != $scope.user.username){
                $scope.active_users.push(data.username);
            }
            break;
        case "client-inactive":
            if($scope.active_users.indexOf(data.username) >= 0){
                $scope.active_users.splice($scope.active_users.indexOf(data.username), 1);
            }
            break;
        case "update-channel-settings":
            var c = $scope.getChannelObjectByName(data.channel_name);
            c.updateSettings(data);
            break;
        case "latest-user-settings":

            break;
    }
};