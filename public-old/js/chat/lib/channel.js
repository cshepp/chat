
function Channel(name, socket){

    this.socket = socket;

    this.name = name;
    this.active_parsers = [];
    this.unread = 0;
    this.joined = false;
    this.messages = [];
    this.show_notifications = false; //0 = none, 1 = all

    this.syncToLocalStorage = function(){

    };

    this.syncToServer = function(){
        this.socket.emit('control-action', { action_type: 'update-channel-settings', channel_name: this.name });
    };

    this.handleServerSync = function(data){

    };
}

Channel.prototype.requestSyncFromServer = function(){
    this.socket.emit('control-action', { action_type: 'request-channel-settings', channel_name: this.name });
};

Channel.prototype.updateSettings = function(data){
    console.log("update channel settings", data);
};