/* Frontend Angular App - CHAT PAGE */

var app = angular.module('app', ['btford.socket-io']);

app.factory('socket', function(socketFactory){
    var remote = "http://localhost:8080";
    var ioSocket = io.connect(remote, { query: 'token=' + localStorage.token });
    return socketFactory({ ioSocket: ioSocket});
});

app.controller('MainCtrl', function MainCtrl ($scope, socket){


    /********************************************
    *                                           *
    *                SOCKETS                    *
    *                                           *
    ********************************************/
    socket.emit('control-action', { action_type: 'register-user', username: localStorage.username });

    socket.on('message', function(data){
        console.log('message:', data);
        var channel = $scope.getChannelObjectByName(data.channel_name);
        channel.messages.push($scope.parseMessage(channel.name, data.message));

        if(data.message.username !== $scope.user.username){
            if($scope.settings_mentions === true && data.message.text.indexOf($scope.user.username) >= 0){
                //mention
                $scope.emitNotification(data.message.username, data.message.text, data.channel_name, data.message.timestamp);
            }
            else if(JSON.parse(localStorage.settings).keywords.length > 0){
                //check for keyword mentions
                var keywords = JSON.parse(localStorage.settings).keywords;
                var notify = false;
                for(var i = 0; i < keywords.length; i++){
                    if(keywords[i] === "") continue;
                    if(data.message.text.indexOf(keywords[i]) >= 0){
                        notify = true;
                        break;
                    }
                }
                if(notify){
                    $scope.emitNotification(data.message.username, data.message.text, data.channel_name, data.message.timestamp);
                }
            }
        }

        if(data.channel_name != $scope.active_channel_name){
            channel.unread += 1;
        }

        $scope.$digest();
        $scope.scrollMessages();
    });

    var messageParser = new MessageParser($scope);

    socket.on('control-action', function(data){
        messageParser.handleControlAction(data);

        $scope.$digest();
        console.log("control-action:", data);
    });
    //end of socket stuff

    /********************************************
    *                                           *
    *                   INIT                    *
    *                                           *
    ********************************************/

    $scope.channels = [];
    $scope.user = new User(localStorage.username);
    $scope.ready = false;

    /********************************************
    *                                           *
    *                  SCOPE                    *
    *                                           *
    ********************************************/


    //@TODO move to separate module
    $scope.new_user_username = "";
    $scope.new_user_password = "";
    $scope.new_user_admin = "";
    $scope.active_users = [];



    $scope.user.settings.auto_join_channels.map(function(chan){
        socket.emit('control-action', { action_type: "join-channel", username: $scope.user.username, channel_name: chan });
    });

    $scope.message_input = "";


    $scope.channels = JSON.parse(localStorage.channels).map(function(chan){
        var c = new Channel(chan, socket);
        c.requestSyncFromServer();
        return c;
    });

    $scope.active_channel_name = "general";

    $scope.switchActiveChannel = function(channel_name){
        $scope.active_channel_name = channel_name;
        var chan = $scope.getChannelObjectByName(channel_name);
        if(!chan.joined){
            socket.emit('control-action', { action_type: "join-channel", username: $scope.user.username, channel_name: channel_name });
        }
        $scope.scrollMessages();
        chan.unread = 0;
    };

    $scope.getChannelClass = function(channel){
        if(channel.name == $scope.active_channel_name){
            return "active-channel";
        }
        else if(channel.joined){
            return "in-channel";
        }

        return "not-in-channel";
    };

    $scope.leaveChannel = function(name){

    };

    $scope.getActiveChannel = function(){
        return $scope.getChannelObjectByName($scope.active_channel_name);
    };

    $scope.getActiveChannelMessages = function(){
        var channel = $scope.getChannelObjectByName($scope.active_channel_name);
        return channel.messages;
    };

    $scope.getChannelObjectByName = function(name){
        for(var i = 0; i < $scope.channels.length; i++){
            if($scope.channels[i].name == name){
                return $scope.channels[i];
            }
        }
    };

    $scope.sendMessage = function(e){
        if(e.keyCode === 13){
            var text = $scope.message_input;
            socket.emit('message', { channel_name: $scope.active_channel_name, message: { username: $scope.user.username, timestamp: Date.now(), text: text }} );
            $scope.message_input = "";
        }
    };

    $scope.addChannel = function(){
        var new_name = prompt("Please enter the new channel's name.");
        if(new_name){
            socket.emit('control-action', { action_type: "create-channel", channel_name: new_name });
            socket.emit('control-action', { action_type: "join-channel", username: $scope.user.username, channel_name: new_name });
        }
    };

    $scope.emitNotification = function(username, message, channel, timestamp){
        //@TODO - does this work? do we need to request access from the user?
        var n = new Notification(message, { icon: "", body: "@" + username + " in #" + channel + " at " + timestamp });
        console.log(n);
    };

    $scope.saveSettings = function(){
        var mentions = $scope.settings_mentions;
        var keywords = $scope.settings_alert_keywords;
        var existing_settings = JSON.parse(localStorage.settings);
        existing_settings.mentions = mentions;
        existing_settings.keywords = keywords.split(',').map(function(str){ return str.trim(); });

        localStorage.setItem('settings', JSON.stringify(existing_settings));
    };

    $scope.createUser = function(){
        if($scope.user.settings.is_admin){
            socket.emit('control-action', { action_type: 'create-user', username: $scope.new_user_username, password: $scope.new_user_password, is_admin: $scope.new_user_admin });
            $scope.new_user_username = "";
            $scope.new_user_password = "";
            $scope.new_user_admin = false;
        }
    };

    /*
        ensures that the _bottom_ message (most recent) is visible
    */
    $scope.scrollMessages = function(){
        objDiv = document.getElementById("message-pane");
        objDiv.scrollTop = objDiv.scrollHeight + 250;
    };

    /*
        
    */
    $scope.loadPreviousMessages = function(){
        //find oldest message for active channel
        var messages = $scope.getActiveChannel().messages;
        var oldest_message = null;
        for(var i = 0; i < messages.length; i++){
            var m = messages[i];
            if(oldest_message === null || m.timestamp < oldest_message.timestamp){
                oldest_message = m;
            }
        }
        //request 10 messages prior to that message for active channel
        socket.emit('control-action', { action_type: 'get-previous-messages', channel_name: $scope.active_channel_name, from: oldest_message.timestamp });
    };

    $scope.parseMessage = function(channel, message){
        var text = message.text;
        message.attachments = [];
        var chan = $scope.getChannelObjectByName(channel);
        //if(chan.allow_images) @TODO - channel restrictions


        //here we're going to check for links/images/redmine tickets
        /* redmine matches */
        var redmine_rx = /(T|t)(icket)(\s+)(#)(\d+)(\s+)?/g;
        var redmine_matches = text.match(redmine_rx);
        if(redmine_matches !== null){
            console.log(redmine_matches);
            for(var j = 0; j < redmine_matches.length; j++){
                var m = redmine_matches[j];
                var link = "http://redmine.webstaurantstore.com/issues/" + m.substr(m.indexOf("#") + 1).trim();
                message.attachments.push({ type: 'plain_link', link: link });
            }
        }

        //'peg' is for catching .jpeg
        var image_extensions = ['png', 'jpg', 'gif', 'peg'];

        /* plain links & image links */
        var url_rx = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
        var url_matches = text.match(url_rx);
        if(url_matches !== null){
            for(var i = 0; i < url_matches.length; i++){
                if(redmine_matches === null || redmine_matches.indexOf(url_matches[i]) < 0){
                    if(image_extensions.indexOf(url_matches[i].substr(url_matches[i].length - 3)) >= 0){
                        message.attachments.push({ type: 'image_link', link: url_matches[i] });
                    }
                    else{
                        message.attachments.push({ type: 'plain_link', link: url_matches[i] });
                    }
                }
            }
        }

        return message;
    };

    $scope.showChannelSettings = function(channel_name){
        var chan = $scope.getChannelObjectByName(channel_name);
        $scope.channel_settings_name = channel_name;
        $scope.channel_settings_show_notifications = chan.show_notifications;

        if($scope.user.settings.is_admin){
            $scope.channel_settings_allow_images = chan.allow_images;
        }
    };

    $scope.saveChannelSettings = function(){
        //@TODO
        var chan = $scope.getChannelObjectByName($scope.channel_settings_name);
        if($scope.channel_settings_show_notifications){
            var channels_with_notifications = user.settings.channels_with_alerts;
            if(channels_with_notifications.indexOf(chan.name) < 0){
                channels_with_notifications.push(chan.name);
                socket.emit('control-action', { type: 'update-user-settings'});
            }
        }

        if(user.settings.is_admin){

        }
    };
});

app.filter('timestampFormat', function(){
    return function(value){
        var date = new Date(value);
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var hour = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();

        var toTwoDigits = function(value){
            var v = value + "";
            if(v.length == 1){
                return "0" + v;
            }

            return v;
        };

        var isToday = function(day, month, year){
            var today = new Date(Date.now());
            return (day == today.getDate() && month == today.getMonth() + 1 && year == today.getFullYear());
        };

        var getTime = function(hour, minutes, seconds){
            return (hour % 12 === 0 ? 12: hour % 12) + ":" + toTwoDigits(minutes) + " " + (hour / 12 >= 1 ? "PM" : "AM");
        };

        if(isToday(day, month, year)){
            return getTime(hour, minutes, seconds);
        }
        else{
            return day + "/" + month + "/" + year + " " + getTime(hour, minutes, seconds);
        }
    };
});



